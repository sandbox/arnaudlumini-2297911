<?php

function facebook_light_fb_init()
{
	global $facebook;
	global $base_url;
	
	if($facebook) return $facebook;

	$fb_lib_path = libraries_get_path('facebook-php-sdk');
	$fb_platform = $fb_lib_path . '/src/facebook.php';
	if (!class_exists('Facebook') && $fb_platform)
		include($fb_platform);

	if (!libraries_load('facebook-php-sdk'))
	{
  		drupal_set_message(t('Facebook library not loaded.'), 'error');
  		watchdog('facebook_light', t('Facebook library not loaded.'), array(), WATCHDOG_ERROR);
	}
	
	$facebook = new Facebook(facebook_light_get_config());
//	if (isset($_COOKIE["access_token"]))
//		$facebook->setAccessToken($_COOKIE["access_token"]);
	return $facebook;
}

function facebook_light_get_environment(){
	global $base_url;
	
	$local_regexp = variable_get('facebook_light_local_regexp', '');
	$dev_regexp = variable_get('facebook_light_dev_regexp', '');
	$demo_regexp = variable_get('facebook_light_demo_regexp', '');
	
	if ($local_regexp != '' && preg_match($local_regexp, $base_url) != false)
		return 'local';
	if ($dev_regexp != '' && preg_match($dev_regexp, $base_url) != false)
		return 'dev';
	if ($demo_regexp != '' && preg_match($demo_regexp, $base_url) != false)
		return 'demo';
	return 'prod';
}

function facebook_light_get_config(){
	$config = array();
	
	if (facebook_light_get_environment() == 'local')
	{
		$config['appId'] = variable_get('facebook_light_local_facebook_appId', '');
		$config['secret'] = variable_get('facebook_light_local_facebook_secret', '');
	}
	else if (facebook_light_get_environment() == 'dev')
	{
		$config['appId'] = variable_get('facebook_light_dev_facebook_appId', '');
		$config['secret'] = variable_get('facebook_light_dev_facebook_secret', '');
	}
	else if (facebook_light_get_environment() == 'demo')
	{
		$config['appId'] = variable_get('facebook_light_demo_facebook_appId', '');
		$config['secret'] = variable_get('facebook_light_demo_facebook_secret', '');
	}
	else
	{
		$config['appId'] = variable_get('facebook_light_prod_facebook_appId', '');
		$config['secret'] = variable_get('facebook_light_prod_facebook_secret', '');
	}
	$config['cookie'] = true;
	$config['fileUpload'] = false; // optional
	return $config;
}

/**
 * Sends app notification.
 * @param fb_id int
 * 		The recipient user id.
 * @param message
 * 		The message to send.
 */
function facebook_light_send_notification($fb_id, $message)
{
	$config = facebook_light_get_config();
	
	$access_token = $config['appId'] . '|' . $config['secret'];
	$template = $message;
	
	$fields = array('access_token' => $access_token, 'template' => $template);
		
	$url = 'https://graph.facebook.com/' . $fb_id . '/notifications?' . http_build_query($fields);
	$header = array();
	$header[] = 'Accept: application/json';
	$header[] = 'Cache-Control: max-age=0';
	$header[] = 'Connection: keep-alive';
	$header[] = 'Keep-Alive: 300';
	$header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
	$header[] = 'Accept-Language: en-us,en;q=0.5';
	$header[] = 'Pragma: ';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
	$return = curl_exec($ch);
	$error = curl_error($ch);
	if ($error)
		watchdog('name_day_sender', 'Failed to send notification "' . $template . '" to ' . $fb_id . ' because of curl: ' .  $error, array(), WATCHDOG_ERROR);
	curl_close($ch);
}

?>

<?php

/*
 * @file
 * Administration form
 */
function facebook_page_settings_form()
{
	$form['tab_url'] = array(
		'#type' => 'fieldset',
		'#title' => 'Tab url',
		'#description' => 'Without protocol, beginning with // (example : //www.facebook.com/pages/Hollywood-demo/592825420729544?id=592825420729544)',
	);
	
	$form['tab_url']['facebook_page_dev_tab_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Dev Tab url'),
		'#default_value' => variable_get('facebook_page_dev_tab_url', ''),
		'#size' => 100,
	);
	
	$form['tab_url']['facebook_page_demo_tab_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Demo Tab url'),
		'#default_value' => variable_get('facebook_page_demo_tab_url', ''),
		'#size' => 100,
	);
	
	$form['tab_url']['facebook_page_prod_tab_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Prod Tab url'),
		'#default_value' => variable_get('facebook_page_prod_tab_url', ''),
		'#size' => 100,
	);
	
	$config = facebook_light_get_config();
	$form['add_tab'] = array(
		'#type' => 'markup',
		'#markup' => l(
			'Add tab',
			'https://www.facebook.com/dialog/pagetab?app_id=' . $config['appId'] . '&display=page&redirect_uri=https://www.facebook.com',
			array(
				'attributes' => array(
					'target' => '_blank'
				),
			)
		),
	);
	
	return system_settings_form($form);
}

?>

<?php

/*
 * @file
 * Administration form
 */
function facebook_light_settings_form()
{
	$form = array ();
	
	$form['facebook'] = array(
		'#type' => 'fieldset',
		'#title' => t('Facebook'),
		'#description' => 'Regexps will be analysed in this order : local => dev => demo. The <b>first match</b> is considered the <b>good one</b>. If <b>no match</b>, the domain is considered <b>production</b>.'
	);
	
	$form['facebook']['local'] = array(
		'#type' => 'fieldset',
		'#title' => t('Local app'),
	);
	
	$form['facebook']['dev'] = array(
		'#type' => 'fieldset',
		'#title' => t('Dev app'),
	);
	
	$form['facebook']['demo'] = array(
		'#type' => 'fieldset',
		'#title' => t('Demo app'),
	);
	
	$form['facebook']['prod'] = array(
		'#type' => 'fieldset',
		'#title' => t('Prod app'),
	);
	
	$form['facebook']['local']['facebook_light_local_regexp'] = array(
		'#type' => 'textfield',
		'#title' => t('Domain name regexp'),
		'#default_value' => variable_get('facebook_light_local_regexp', '#.local.|-local.#'),
		'#required' => TRUE
	);
	$form['facebook']['local']['facebook_light_local_facebook_appId'] = array(
		'#type' => 'textfield',
		'#title' => t('AppId'),
		'#default_value' => variable_get('facebook_light_local_facebook_appId', ''),
		'#required' => TRUE
	);
	$form['facebook']['local']['facebook_light_local_facebook_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('Secret'),
		'#default_value' => variable_get('facebook_light_local_facebook_secret', ''),
		'#required' => TRUE
	);
	
	$form['facebook']['dev']['facebook_light_dev_regexp'] = array(
		'#type' => 'textfield',
		'#title' => t('Domain name regexp'),
		'#default_value' => variable_get('facebook_light_dev_regexp', '#.dev.|-dev.#'),
		'#required' => TRUE
	);
	$form['facebook']['dev']['facebook_light_dev_facebook_appId'] = array(
		'#type' => 'textfield',
		'#title' => t('AppId'),
		'#default_value' => variable_get('facebook_light_dev_facebook_appId', ''),
		'#required' => TRUE
	);
	$form['facebook']['dev']['facebook_light_dev_facebook_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('Secret'),
		'#default_value' => variable_get('facebook_light_dev_facebook_secret', ''),
		'#required' => TRUE
	);
	
	$form['facebook']['demo']['facebook_light_demo_regexp'] = array(
		'#type' => 'textfield',
		'#title' => t('Domain name regexp'),
		'#default_value' => variable_get('facebook_light_demo_regexp', '#.demo.|-demo.|-recette#'),
		'#required' => TRUE
	);
	$form['facebook']['demo']['facebook_light_demo_facebook_appId'] = array(
		'#type' => 'textfield',
		'#title' => t('AppId'),
		'#default_value' => variable_get('facebook_light_demo_facebook_appId', ''),
	);
	$form['facebook']['demo']['facebook_light_demo_facebook_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('Secret'),
		'#default_value' => variable_get('facebook_light_demo_facebook_secret', ''),
	);
	
	$form['facebook']['prod']['facebook_light_prod_facebook_appId'] = array(
		'#type' => 'textfield',
		'#title' => t('AppId'),
		'#default_value' => variable_get('facebook_light_prod_facebook_appId', ''),
	);
	$form['facebook']['prod']['facebook_light_prod_facebook_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('Secret'),
		'#default_value' => variable_get('facebook_light_prod_facebook_secret', '')
	);
	
	$form['facebook']['facebook_light_add_js_sdk_synchronously'] = array(
		'#type' => 'checkbox',
		'#title' => t('Add JS SDK synchronously.'),
		'#default_value' => variable_get('facebook_light_add_js_sdk_synchronously', 1),
	);

	return system_settings_form($form);
}

?>

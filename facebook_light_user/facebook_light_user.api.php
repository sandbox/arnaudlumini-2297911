<?php

/*
 * deprecated $last_name, $first_name, $fb_id
 * OR
 * $me
 */
function facebook_light_user_create_user()
{
	$account = new stdClass();
	
	$args_count = func_num_args();
	//case to handle deprecated signature
	if ($args_count == 3)
	{
		$account->name = func_get_arg(2);
		$account->field_fb_last_name[LANGUAGE_NONE][0]['value'] = func_get_arg(0);
		$account->field_fb_first_name[LANGUAGE_NONE][0]['value'] = func_get_arg(1);
		$account->field_fb_id[LANGUAGE_NONE][0]['value'] = func_get_arg(2);
	}
	else if ($args_count == 1)
	{
		$me = func_get_arg(0);
		$account->name = $me['id'];
		$account->field_fb_last_name[LANGUAGE_NONE][0]['value'] = $me['last_name'];
		$account->field_fb_first_name[LANGUAGE_NONE][0]['value'] = $me['first_name'];
		$account->field_fb_id[LANGUAGE_NONE][0]['value'] = $me['id'];
		$account->field_fb_gender[LANGUAGE_NONE][0]['value'] = $me['gender'];
		
		$account->picture = system_retrieve_file('http://graph.facebook.com/' . $me['id'] . '/picture?type=large', null, true);
	}
	
	$user = user_save($account);
	
	return $user;
}

function facebook_light_user_is_allowed_user($nid)
{
	//initializing facebook
	$facebook = facebook_light_fb_init();
	$fb_id = $facebook->getUser();
	
	if ($fb_id == 0)
		return false;
	
	//getting user
	$query = new EntityFieldQuery();
	$entities = $query->entityCondition('entity_type', 'user', '=')
		->fieldCondition('field_fb_id', 'value', $fb_id, '=')
		->range(0,1)
		->execute();
	
	//if user exists
	if (isset($entities['user']))
	{
		foreach ($entities['user'] as $account)  
			$uid = $account->uid;
		$node = node_load($nid);
		if ($node->uid == $uid)
			return true;
	}
	return false;
}

function facebook_light_user_get_current_user()
{
	//initializing facebook
	$facebook = facebook_light_fb_init();
	
	$fb_id = $facebook->getUser();
	
	if ($fb_id == 0)
		return 0;
	
	return facebook_light_user_get_user_by_fb_id($fb_id);
}

function facebook_light_user_get_user_by_fb_id($fb_id)
{
	//getting user
	$query = new EntityFieldQuery();
	$entities = $query->entityCondition('entity_type', 'user', '=')
		->fieldCondition('field_fb_id', 'value', $fb_id, '=')
		->range(0,1)
		->execute();
	
	//if user exists
	if (isset($entities['user']))
	{
		foreach ($entities['user'] as $account)
			$current_uid = $account->uid;
		return $current_uid;
	}
	return 0;
}

function facebook_light_user_get_user_friends()
{
	//initializing facebook
	$facebook = facebook_light_fb_init();
	$fb_id = $facebook->getUser();
	
	if ($fb_id == 0)
		return array();
	
	$friends = $facebook->api('/' . $fb_id . '/friends?fields=id');
	//getting users
	if(!empty($friends['data']))
	{
		$query = new EntityFieldQuery();
		$entities = $query->entityCondition('entity_type', 'user', '=')
		->fieldCondition('field_fb_id', 'value', 'NULL', '!=')
		->fieldCondition('field_fb_id', 'value', $friends['data'], 'IN')
		->execute();
	}

	$friends_uids = array();
	//if user exists
	if (isset($entities['user']))
	{
		foreach ($entities['user'] as $account)
			$friends_uids[] = $account->uid;
	}
	return $friends_uids;
}

?>

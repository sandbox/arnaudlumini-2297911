<?php

function _facebook_light_user_resource_definition()
{
	$definition = array(
		'facebook_light_user' => array(
			'actions' => array(
				'save' => array(
					'help' => 'Deprecated. Saves a facebook light user from server side information. If already exists, does nothing.',
					'file' => array('type' => 'inc', 'module' => 'facebook_light_user', 'name' => 'resources/facebook_light_user_resource'),
					'callback' => '_facebook_light_user_resource_save',
					'access callback' => '_facebook_light_user_resource_access',
					'args' => array(
						array(
							'name' => 'scope',
							'optional' => TRUE,
							'type' => 'string',
							'description' => 'Comma separated list of permissions. Supported ones are: email,user_location.',
							'source' => array('data' => 'scope'),
						),
					),
				),
				'login' => array(
					'help' => 'Logs a user in from Facebook server side information. Creates user on the fly if he doesn\'t exist.',
					'file' => array('type' => 'inc', 'module' => 'facebook_light_user', 'name' => 'resources/facebook_light_user_resource'),
					'callback' => '_facebook_light_user_resource_login',
					'access callback' => '_facebook_light_user_resource_access',
					'args' => array(
						array(
							'name' => 'scope',
							'optional' => TRUE,
							'type' => 'string',
							'description' => 'Comma separated list of permissions. Supported ones are: email,user_location.',
							'source' => array('data' => 'scope'),
						),
					),
				),
			),
		),
	);

	return $definition;
}

function _facebook_light_user_resource_login($scope = null)
{
	$facebook = facebook_light_fb_init();
	$fb_id = $facebook->getUser();
	$uid = facebook_light_user_get_user_by_fb_id($fb_id);
	
	if ($uid == 0)
	{
		$me = $facebook->api('/' . $fb_id . '?fields=picture,about,last_name,first_name,gender');
		$account = facebook_light_user_create_user($me);
		
		$edit = array();
		
		if (isset($scope))
		{
			if (in_array('email', explode(',', $scope)))
			{
				$email = $facebook->api('/me?fields=email');
				$email = $email['email'];
				$edit['mail'] = $email;
			}
			if (in_array('user_location', explode(',', $scope)))
			{
				if (isset($me['location']))
					$edit['field_fb_city'][LANGUAGE_NONE][0]['value'] = $me['location']['name'];
				else
					$edit['field_fb_city'][LANGUAGE_NONE][0]['value'] = '';
			}
		}
		
		drupal_alter('facebook_light_user_resource_save', $edit);
		
		$account->status = 1;
		$account = user_save($account, $edit);
	}
	else
	{
		$account = user_load($uid);
	}
	
	$form_state['uid'] = $account->uid;
	user_login_submit(array(), $form_state);
	
	$return = new stdClass();
	$return->user = $account;
	$return->token = drupal_get_token('services');
	return $return;
}

function _facebook_light_user_resource_save($scope = null)
{
	$facebook = facebook_light_fb_init();
	$fb_id = $facebook->getUser();
	$uid = facebook_light_user_get_user_by_fb_id($fb_id);
	
	if ($uid == 0)
	{
		$me = $facebook->api('/' . $fb_id);
		$account = facebook_light_user_create_user($me);
		
		$edit = array();
		
		//if picture
		$picture = $facebook->api('/me/?fields=picture');
		$path = $picture['picture']['data']['url'];
		$edit['picture'] = system_retrieve_file('http://graph.facebook.com/'.$fb_id.'/picture?type=large', null, true);
		
		if (isset($scope))
		{
			if (in_array('email', explode(',', $scope)))
			{
				$email = $facebook->api('/me?fields=email');
				$email = $email['email'];
				$edit['mail'] = $email;
			}
			if (in_array('user_location', explode(',', $scope)))
			{
				$edit['field_fb_city'][LANGUAGE_NONE][0]['value'] = $me['location']['name'];
			}
		}
		
		drupal_alter('facebook_light_user_resource_save', $edit);
		
		$user = user_save($account, $edit);
		return $user;
	}
	return true;
}

function _facebook_light_user_resource_access()
{
	return true;
}
